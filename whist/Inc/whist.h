#pragma once
#include "main.h"
//#include "stm32l4xx_hal.h"
//#include "stm32l4xx_hal_conf.h"

#define A7 GPIOA, (1<<2)
#define D3 GPIOB, (1<<0)
#define D6 GPIOB, (1<<1)
#define D8 GPIOC, (1<<15) // seemingly not connected
#define D9 GPIOA, (1<<8)
#define D10 GPIOA, (1<<11)

int printf2(const char *format, ...);
void my_assert(uint32_t ok);

/* Typical usage:
Define global (or static local) var:
	heart_t myheart = {0, 44100, D9};
Update:
	heart_beat(&myheart);
	
Caller is responsible for setting up output pins	
*/
	
typedef struct {
	uint32_t count;
	uint32_t thresh;
	GPIO_TypeDef *GPIOx;
	uint32_t PinMask;
} heart_t;

void heart_beat(heart_t* heart);

