#include "main.h"

#include <stdbool.h>
#include <string.h>

extern I2C_HandleTypeDef hi2c1;
//extern DMA_HandleTypeDef hdma_i2c1_tx;

#define SID 0x70

static uint8_t grid[16];



void my_assert(uint32_t ok);



void ledmat_set(uint8_t row, uint8_t col, int on)
{
	// the LED has a strange co-ordinate system
	int pos1 = col*2 +1; // fill in the odd-numbered indices. The even s/b 0 (they're for colour)
	int pos2 = (row + 7) %8; // the device seems to have a crazy grid system

	if(on)
		grid[pos1] |= (1<<pos2); // set bit
	else
		grid[pos1] &= ~(1<<pos2); // clear bit
}

void led_set_row(uint8_t row, uint8_t value){
	for (int c = 0; c < 8; ++c)
		ledmat_set(row, c, value & (1<<c));
}


void ledmat_bsp_write(uint8_t *data, int n)
{
	extern void ledmat_bitbang_write(uint8_t sid, uint8_t *data, int n);
	ledmat_bitbang_write(SID, data, n);
}

void ledmat_init(void)
{

	memset(grid, 0, 16);
	uint8_t cmds[] = {
			0x20 | 1, // turn on oscillator
			0x81, // display on
			0xe0 | 0 // brightness to minimum
	};
	for(int i =0; i < sizeof(cmds); i++) {
		ledmat_bsp_write(cmds+i, 1);
		//while(I2C1->ISR & I2C_ISR_BUSY);
	}
}


void ledmat_show()
{
	ledmat_bsp_write((uint8_t*) grid, 16);
}

static uint8_t  pattern1[] = {
	0b10000001,
	0b01000010,
	0b00100100,
	0b00010000,

	0b00001000,
	0b00100100,
	0b01000010,
	0b10000001
};

static uint8_t  pattern2[] = { // the letter P, with some orientation frills
	0b11110001,
	0b10001000,
	0b10001000,
	0b11110000,

	0b10000000,
	0b10000000,
	0b10000001,
	0b10000010
};

void my_assert(uint32_t ok)
{
	if(ok) return;
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, GPIO_PIN_SET);
	while(1);
}

void delayish(int n) { HAL_Delay(n); }

void app(void)
{
	//my_assert(false);
	uint8_t* pattern = pattern1;
	ledmat_init();
	while(1) {
		static int offset = 0; // for scrolling purposes
		for (int r = 0; r < 8; ++r) {
			int r1 = (r + offset) %8;
			led_set_row(r1, pattern[r]);
		}
		offset++;
		ledmat_show();
		delayish(100);
		//nop();
	}

}
