#include "main.h"

volatile int count = 0;

void print_count(void)
{
	char msg[100];
	//static int count = 0;
	int l_count = count;
	static int idx = 0;
	count = 0;
	int n = snprintf(msg, 100, "Idx: %d, count: %d\n", idx++, l_count);
	HAL_UART_Transmit_DMA(&huart2, (uint8_t*) msg, n);
}

void app(void)
{

  //HAL_RNG_GenerateRandomNumber_IT(&hrng);
  RNG->CR |= RNG_CR_RNGEN; // enable range generation
  RNG->CR |= RNG_CR_IE; // enable interrupts

  //RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;
  TIM2->ARR = SystemCoreClock/1 -1; // every second
  //TIM2->EGR
  //TIM2->DIER |= TIM_DIER_UIE; // enable update interrupt
  LL_TIM_EnableIT_UPDATE(TIM2);
  //TIM2->CR1 |= TIM_CR1_CEN; // enable timer counter.
  LL_TIM_EnableCounter(TIM2);
  //TIM2->EGR |= TIM_EGR_UG; // generate update even to reset timer and apply settings

  //SET_BIT(TIM2->CR1, TIM_CR1_CEN);



  while(1);
}
