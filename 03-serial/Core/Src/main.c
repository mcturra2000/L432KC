/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//#include <stdio.h>
#include <stdarg.h>
#include "delay.h"
#include "gpio.h"
#include "uart.h"
#include "jg_printf.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
//static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#if 0
int printf2(const char *format, ...)
{
	char msg[132];
	va_list ap;
	va_start(ap, format);
	//int n = vsnprintf(msg, sizeof(msg), format, ap);
	va_end(ap);

	int n = jg_snprintf(msg, sizeof(msg), "hello %d\n", 66);

	uart2_send_blocking(msg, n);
	/*
	USART2->CR1 |= USART_CR1_TE; //send an idle frame
	char *str = msg;
	while(n--) {
		while ((USART2->ISR & USART_ISR_TXE) == 0); // wait until transmission buffer is empty
		USART2->TDR = *str++; // send a char
	}
	while((USART2->ISR & USART_ISR_TC) == 0); // wait until transmission complete
*/

	return n;
}
#endif

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
#if 0
static void MX_USART2_UART_Init_Test(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */





  /* Peripheral clock enable */
#if 0
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART2);
#else
  RCC->APB1ENR1 |= RCC_APB1ENR1_USART2EN;
#endif

#if 0
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);
  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
  /**USART2 GPIO Configuration
  PA2   ------> USART2_TX
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_2;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_7;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);
#else
  gpio_pullup(GPIOA, 2);
  gpio_alt(GPIOA, 2, 7);
#endif

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
#if 0
  LL_USART_InitTypeDef USART_InitStruct = {0};
  USART_InitStruct.BaudRate = 115200;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B; // auto
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1; // auto
  USART_InitStruct.Parity = LL_USART_PARITY_NONE; // auto
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;  // auto
  LL_USART_Init(USART2, &USART_InitStruct);
  LL_USART_ConfigHalfDuplexMode(USART2);
  LL_USART_Enable(USART2);
#else
	uint16_t uartdiv = SystemCoreClock / 115200;
	USART2->BRR = ( ( ( uartdiv / 16 ) << USART_BRR_DIV_MANTISSA_Pos ) |
			( ( uartdiv % 16 ) << USART_BRR_DIV_FRACTION_Pos ) );
  //USART2->BRR = 115200;
  USART2->CR1 |= USART_CR1_TE; // enable transmission
  USART2->CR1 |= USART_CR1_UE; // enable uart itself
#endif

  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}
#endif


#if 0
// override the weak function of syscalls.c
int __io_putchar(int ch)
{
	//uint8_t c= ch;
	//HAL_UART_Transmit(&huart2, &c, 1, 100);
	//LL_USART_TransmitData8(USART2, ch);
	//LL_USART
	return 1;
}
#endif

#if 0
static inline void __attribute__((optimize("O0"))) nop(void)
{
	asm volatile("nop");
}

void  __attribute__((optimize("O0"))) nops(uint32_t n)
{
	for(uint32_t i=0; i<n; i++)
		nop();
}

/* approx delay in ms without systick */
void __attribute__((optimize("O0"))) delayish (uint32_t ms)
{
	//uint32_t i, j;
	uint32_t factor = SystemCoreClock / 16000 * 10 /15; // calibrated using logic analyser
	for(uint32_t i=0; i<ms; i++)
		nops(factor);
}
#endif
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */

  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

  NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  /* System interrupt init*/

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  //MX_USART2_UART_Init_Test();
  /* USER CODE BEGIN 2 */

uart2_init();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  //char msg[100];
  int count = 0;

  while (1)
  {
	  printf2("Count : %d\n", count++);
	  //int n = sprintf(msg, "Count=%d\n", count++);
	  //printf("hello world\n");
	  //HAL_UART_Transmit(&huart2, (uint8_t *) msg, n, 100);
	  //HAL_Delay(1000);
	  delayish(1000);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_4);
  while(LL_FLASH_GetLatency()!= LL_FLASH_LATENCY_4)
  {
  }
  LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE1);
  LL_RCC_MSI_Enable();

   /* Wait till MSI is ready */
  while(LL_RCC_MSI_IsReady() != 1)
  {

  }
  LL_RCC_MSI_EnableRangeSelection();
  LL_RCC_MSI_SetRange(LL_RCC_MSIRANGE_6);
  LL_RCC_MSI_SetCalibTrimming(0);
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_MSI, LL_RCC_PLLM_DIV_1, 40, LL_RCC_PLLR_DIV_2);
  LL_RCC_PLL_EnableDomain_SYS();
  LL_RCC_PLL_Enable();

   /* Wait till PLL is ready */
  while(LL_RCC_PLL_IsReady() != 1)
  {

  }
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

   /* Wait till System clock is ready */
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
  {

  }
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);

  LL_Init1msTick(80000000);

  LL_SetSystemCoreClock(80000000);
  LL_RCC_SetUSARTClockSource(LL_RCC_USART2_CLKSOURCE_PCLK1);
}


/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
