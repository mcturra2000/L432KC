//Refs: db7.29, blog of 2022-02-17

#include "main.h"

//#include <assert.h>

//void init_big_ball_of_mud(void);
//extern TIM_HandleTypeDef htim1;

//extern DAC_HandleTypeDef hdac1;

const float fs = 44000.0; // sample rate
const float fw = 500.0; // frequency of wave;
const float pi2 = 6.283185307179586;
float dx = 0; // pi2 * fw/fs;

// sine wave
int get1(void)
{
	volatile static float x = 0.0, y = 0.0;
	x += dx;
	if(x>pi2) x -= pi2;
	y = (SinHP(x) + 1.0) * 4095.0/2.0;
	return y;
}

// saw
uint32_t get2(void)
{
	const uint32_t dy = ((1<<30)/44000)*2 * 500;
	volatile static uint32_t y = 0;
	y+= dy;
	const int nbits = 12; // it's a 12-bit DAC
	return y >> (31-nbits);
}


// not currently used, but we could do.
void TIM1_UP_TIM16_IRQHandler(void)
{
	if(TIM1->SR & TIM_SR_UIF) TIM1->SR = ~TIM_SR_UIF;
	volatile static uint16_t y = 0;
	//return;
	//HAL_DAC_SetValue(&hdac1, DAC_CHANNEL_1, DAC_ALIGN_12B_R, count++);
	LL_GPIO_TogglePin(A1);
	LL_GPIO_SetOutputPin(A0);
	DAC1->DHR12R1 = y;
	y = get2();
	LL_GPIO_ResetOutputPin(A0);
}

void tim1_enable(void) {
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
	TIM1->ARR = (SystemCoreClock / fs -1) * 47747/44000; // with recalibration
	//assert_param(TIM1->ARR < (1<<16));

	TIM1->EGR |= TIM_EGR_UG; // generate update even to reset timer and apply settings
	TIM1->CR1 |= TIM_CR1_CEN; // enable timer counter
	TIM1->DIER |= TIM_DIER_UIE; // enable update interrupt
	NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);
}




void app(void)
{
	dx = pi2 * fw/fs;

	// Setup DAC db07.54
	RCC->APB1ENR1 |= RCC_APB1ENR1_DAC1EN; // Enable DAC clock
	DAC1->CR |= DAC_CR_EN1; // Enable DAC

	tim1_enable();

	while(1);
}
