#pragma once
#include <inttypes.h>
#include <stdbool.h>

#define CB_CAP 128 // capacity
uint32_t cb_data[CB_CAP];
volatile int cb_read_index;
volatile int cb_write_index;
volatile int cb_size;

bool cb_write(uint32_t value);
bool cb_read(uint32_t* value);
void cb_init(void);
