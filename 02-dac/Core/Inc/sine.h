#pragma once

// From
// http://www.mclimatiano.com/faster-sine-approximation-using-quadratic-curve/

// // doesn't seem better than normal sin

// Low Precision version
float SinLP(float x); // low-precision version
float SinHP( float x ); // high precision version
