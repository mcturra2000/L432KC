#include "sine.h"

// From
// http://www.mclimatiano.com/faster-sine-approximation-using-quadratic-curve/

// // doesn't seem better than normal sin

// Low Precision version
float SinLP(float x)
{
	if (x < -3.14159265f)
		x += 6.28318531f;
	else if (x >  3.14159265f)
		x -= 6.28318531f;

	if ( x < 0 )
		return x * ( 1.27323954f + 0.405284735f * x );
	else
		return x * ( 1.27323954f - 0.405284735f * x );
}

// High Precision version
float SinHP( float x )
{
        float sin = 0;
        if (x < -3.14159265f)
                x += 6.28318531f;
        else
                if (x >  3.14159265f)
                        x -= 6.28318531f;

        if ( x < 0 )
        {
                sin = x * ( 1.27323954f + 0.405284735f * x );

                if ( sin < 0 )
                        sin = sin * ( -0.255f * ( sin + 1 ) + 1 );
                else
                        sin = sin * ( 0.255f * ( sin - 1 ) + 1 );
        }
        else
        {
                sin = x * ( 1.27323954f - 0.405284735f * x );

                if ( sin < 0 )
                        sin = sin * ( -0.255f * ( sin + 1 ) + 1 );
                else
                        sin = sin * ( 0.255f * ( sin - 1 ) + 1 );
        }

        return sin;

}
