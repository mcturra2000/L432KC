/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include "whist.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
//volatile uint16_t rx=0, tx=0;

void SPI1_IRQHandler(void)
{
  /* USER CODE BEGIN SPI1_IRQn 0 */
	SPI1->SR = 0;
	//LL_GPIO_SetOutputPin(D9);

	uint16_t val = SPI1->DR;
	//NVIC_DisableIRQ(TIM2_IRQn);
	//DAC1->DHR12R1  = val;
	cb_write(val);
	//NVIC_EnableIRQ(TIM2_IRQn);
	my_assert(val<4096);
	if(cb_size>CB_CAP*3/4)
		LL_GPIO_ResetOutputPin(RDY);

}

void TIM2_IRQHandler(void)
{
	//uint32_t flags = TIM2->SR;
	TIM2->SR = 0;
	//LL_GPIO_TogglePin(D8);
	//if(flags & TIM_SR_CC1IF) {
		LL_GPIO_SetOutputPin(D6);
		uint32_t val;
		cb_read(&val);
		DAC1->DHR12R1 =val;
		if(cb_size<CB_CAP/4) LL_GPIO_SetOutputPin(A7);
	//}
#if 0
	if(flags & TIM_SR_CC2IF) {
		//if(cb_size<CB_CAP/4)	LL_GPIO_SetOutputPin(A7);
		LL_GPIO_ResetOutputPin(D6);
		//LL_GPIO_TogglePin(D8);
	}
#endif

}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */

  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

  NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  /* System interrupt init*/

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */
  cb_init();
  //HAL_SPI_TransmitReceive_DMA(&hspi3, (uint8_t*)&tx, (uint8_t*)&rx, 1);

  RCC->APB1ENR1 |= RCC_APB1ENR1_DAC1EN;
  DAC1->CR |= DAC_CR_EN1;
  //LL_DAC_Enable(DAC1, LL_DAC_CHANNEL_1);

  // TIM2 ENABLE
  //LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM2);
  RCC->APB1ENR1 |= RCC_APB1ENR1_TIM2EN;
  int fs = 44100; // sample rate
  TIM2->ARR = SystemCoreClock/fs-1;
  TIM2->EGR |= TIM_EGR_UG; // apply settings
  TIM2->CR1 |= TIM_CR1_CEN; // enable the counter
  TIM2->DIER |= TIM_DIER_UIE; // enable update interrupt
#if 0
  TIM2->CCR1 = 0;
  LL_TIM_EnableIT_CC1(TIM2);
  int delay_us = 20; // we also trigger a little later (possibly down to 5us)
  TIM2->CCR2 =   SystemCoreClock / 1000000 * delay_us;
  LL_TIM_EnableIT_CC2(TIM2);
#endif
  //LL_TIM_EnableIT_UPDATE(TIM2);
  NVIC_EnableIRQ(TIM2_IRQn);
  //LL_TIM_EnableCounter(TIM2);


  // SPI1 SETUP AS RECEIVE-ONLY SLAVE
  void spi_pin(GPIO_TypeDef *GPIOx, uint32_t pin)
  {
	  LL_GPIO_SetPinMode(GPIOx, pin, LL_GPIO_MODE_ALTERNATE);
	  LL_GPIO_SetPinSpeed(GPIOx, pin, LL_GPIO_SPEED_FREQ_VERY_HIGH);
	  LL_GPIO_SetAFPin_0_7(GPIOx, pin, LL_GPIO_AF_5);
  }

  RCC->APB2ENR |= RCC_APB2ENR_SPI1EN; // enable the spi clock
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOAEN | RCC_AHB2ENR_GPIOBEN; // enable GPIO A and B clocks
  spi_pin(GPIOA, LL_GPIO_PIN_1); // SCK
  spi_pin(GPIOA, LL_GPIO_PIN_7); // MOSI
  spi_pin(GPIOB, LL_GPIO_PIN_0); // NSS
  SPI1->CR1 |= SPI_CR1_RXONLY; // receive only
  LL_SPI_SetDataWidth(SPI1, LL_SPI_DATAWIDTH_16BIT);
  //MX_SPI1_Init();
  NVIC_EnableIRQ(SPI1_IRQn);
  LL_SPI_Enable(SPI1);
  LL_SPI_EnableIT_RXNE(SPI1);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */


  while (1)
  {


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_4);
  while(LL_FLASH_GetLatency()!= LL_FLASH_LATENCY_4)
  {
  }
  LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE1);
  LL_RCC_MSI_Enable();

   /* Wait till MSI is ready */
  while(LL_RCC_MSI_IsReady() != 1)
  {

  }
  LL_RCC_MSI_EnableRangeSelection();
  LL_RCC_MSI_SetRange(LL_RCC_MSIRANGE_6);
  LL_RCC_MSI_SetCalibTrimming(0);
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_MSI, LL_RCC_PLLM_DIV_1, 40, LL_RCC_PLLR_DIV_2);
  LL_RCC_PLL_EnableDomain_SYS();
  LL_RCC_PLL_Enable();

   /* Wait till PLL is ready */
  while(LL_RCC_PLL_IsReady() != 1)
  {

  }
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

   /* Wait till System clock is ready */
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
  {

  }
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);

  LL_Init1msTick(80000000);

  LL_SetSystemCoreClock(80000000);
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
void MX_GPIO_Init(void)
{
  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOC);
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOA);
  LL_AHB2_GRP1_EnableClock(LL_AHB2_GRP1_PERIPH_GPIOB);

  /**/
  LL_GPIO_ResetOutputPin(GPIOC, LL_GPIO_PIN_15);

  /**/
  LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_1);

  /**/
  LL_GPIO_ResetOutputPin(GPIOA, LL_GPIO_PIN_8|LL_GPIO_PIN_11);

  /**/
  LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_2);

  /**/
  GPIO_InitStruct.Pin = LL_GPIO_PIN_15;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /**/
  GPIO_InitStruct.Pin = LL_GPIO_PIN_2;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /**/
  GPIO_InitStruct.Pin = LL_GPIO_PIN_1;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /**/
  GPIO_InitStruct.Pin = LL_GPIO_PIN_8|LL_GPIO_PIN_11;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
//hello from mc
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
