#include "main.h"
#include "ssd1306.h"

extern I2C_HandleTypeDef hi2c1;
extern DMA_HandleTypeDef hdma_i2c1_tx;

void i2c_transfer7(uint32_t i2c, uint8_t addr, const uint8_t *w, size_t wn, uint8_t *r, size_t rn)
{
	if(wn) HAL_I2C_Master_Transmit(&hi2c1, (addr<<1), (uint8_t *)w, wn, 100);
	if(rn) HAL_I2C_Master_Receive(&hi2c1, (addr<<1) | 1 , r, rn, 100);
}

void app(void)
{
	GPIOB->OTYPER |= (1<<6) | (1<<7); // configure for open drain. didn't help
	init_display(0, 64);
	//clear_scr();
	int count = 0;
	//ssd1306_print("Hello world");
	while(1) {
		ssd1306_printf("Count: %d\n", count++);
		//for(int i = 0; i < 16*8; i++) ssd1306_display_cell();
		show_scr();
		HAL_Delay(1000);
	}
}
