#include "main.h"

#include "debounce.h"
#include "jg_printf.h"
#include "uart.h"
#include "gpio.h"

deb_t deb;

void my_systick_handler()
{
	if(HAL_GetTick() %4 == 0) {
		deb_update(&deb, LL_GPIO_IsInputPinSet(GPIOB, GPIO_PIN_4));
		//deb_update(&deb, gpio_get(GPIOB, 4));
	}
}
void app(void)
{
	uart2_init();
	printf2("Cube debounce demo\n");
	deb.en = 1;

	int count = 0;
	while(1) {
		if(deb_rising(&deb))
			printf2("%d: button released\n", count++);
		if(deb_falling(&deb))
			printf2("%d: button pressed\n", count++);

	  if(LL_GPIO_IsInputPinSet(GPIOB, GPIO_PIN_4))
		  LL_GPIO_SetOutputPin(GPIOB, GPIO_PIN_3);
	  else
		  LL_GPIO_ResetOutputPin(GPIOB, GPIO_PIN_3);
	}
}
