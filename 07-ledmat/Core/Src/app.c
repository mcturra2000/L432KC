#include "main.h"

//#include <stm32l4xx_hal_i2c.h>
#include <stdbool.h>
#include <string.h>

extern I2C_HandleTypeDef hi2c1;
extern DMA_HandleTypeDef hdma_i2c1_tx;

#define SID 0x70

static uint8_t grid1[16];

bool use_dma = true;


void my_assert(uint32_t ok);

HAL_StatusTypeDef HAL_I2C_Master_Transmit_DMA_alt(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size);

void I2C_TransferConfig(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t Size, uint32_t Mode,
                               uint32_t Request)
{
  /* Check the parameters */
  assert_param(IS_I2C_ALL_INSTANCE(hi2c->Instance));
  assert_param(IS_TRANSFER_MODE(Mode));
  assert_param(IS_TRANSFER_REQUEST(Request));

  /* update CR2 register */
  MODIFY_REG(hi2c->Instance->CR2,
             ((I2C_CR2_SADD | I2C_CR2_NBYTES | I2C_CR2_RELOAD | I2C_CR2_AUTOEND | \
               (I2C_CR2_RD_WRN & (uint32_t)(Request >> (31U - I2C_CR2_RD_WRN_Pos))) | I2C_CR2_START | I2C_CR2_STOP)), \
             (uint32_t)(((uint32_t)DevAddress & I2C_CR2_SADD) |
                        (((uint32_t)Size << I2C_CR2_NBYTES_Pos) & I2C_CR2_NBYTES) | (uint32_t)Mode | (uint32_t)Request));
}

void I2C_Enable_IRQ(I2C_HandleTypeDef *hi2c, uint16_t InterruptRequest);

void ledmat_bsp_write(uint8_t *data, int n)
{

		while(I2C1->ISR & I2C_ISR_BUSY);
		//my_assert((I2C1->ISR & I2C_ISR_BUSY)== 0);
#if 1
		HAL_StatusTypeDef stat = HAL_I2C_Master_Transmit_DMA(&hi2c1, SID<<1, data, n);
		//my_assert(stat == HAL_OK);
#else
		// doesn't work
		static int init = 0;
		if(init == 0) {
			init = 1;
			LL_DMA_SetPeriphAddress(DMA1, LL_DMA_CHANNEL_6, (uint32_t)&I2C1->TXDR);
			LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_6);
			LL_I2C_EnableIT_TX(I2C1);
			LL_I2C_EnableIT_TC(I2C1);
			LL_I2C_Enable(I2C1);
		}
		  LL_DMA_SetMemoryAddress(DMA1, LL_DMA_CHANNEL_6, (uint32_t)(data));
		  LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_6, n);
			LL_I2C_EnableDMAReq_TX(I2C1);

			/*
		//HAL_DMA_Start(hdma, SrcAddress, DstAddress, DataLength)
		HAL_DMA_Start(hdma, data, DstAddress, DataLength)
		HAL_DMA_Start_IT(&hdma_i2c1_tx, SID<<1, (uint32_t) data, n);
		//I2C_TransferConfig(hi2c, DevAddress, (uint8_t)hi2c->XferSize, xfermode, I2C_GENERATE_START_WRITE);
		I2C_TransferConfig(&hi2c1, SID<<1, n, HAL_I2C_MODE_MASTER, I2C_GENERATE_START_WRITE );
		 __HAL_UNLOCK(&hi2c1);
		#define I2C_XFER_ERROR_IT       (uint16_t)(0x0010U)   //
		I2C_Enable_IRQ(&hi2c1,I2C_XFER_ERROR_IT);
		I2C1->CR1 |= I2C_CR1_TXDMAEN;
		HAL_Delay(20);
		*/
#endif
}

void ledmat_set(uint8_t row, uint8_t col, int on)
{
	// the LED has a strange co-ordinate system
	int pos1 = col*2 +1; // fill in the odd-numbered indices. The even s/b 0 (they're for colour)
	int pos2 = (row + 7) %8; // the device seems to have a crazy grid system

	if(on)
		grid1[pos1] |= (1<<pos2); // set bit
	else
		grid1[pos1] &= ~(1<<pos2); // clear bit
}

void led_set_row(uint8_t row, uint8_t value){
	for (int c = 0; c < 8; ++c)
		ledmat_set(row, c, value & (1<<c));
}


void ledmat_init(void)
{

	//HAL_DMA_
	memset(grid1, 0, 16);
	uint8_t cmds[] = {
			0x20 | 1, // turn on oscillator
			0x81, // display on
			0xe0 | 0 // brightness to minimum
	};
	for(int i =0; i < sizeof(cmds); i++) {
		ledmat_bsp_write(cmds+i, 1);
		//while(I2C1->ISR & I2C_ISR_BUSY);
	}
}


void ledmat_show()
{
	ledmat_bsp_write((uint8_t*) grid1, 16);
}

static uint8_t  pattern1[] = {
	0b10000001,
	0b01000010,
	0b00100100,
	0b00010000,

	0b00001000,
	0b00100100,
	0b01000010,
	0b10000001
};

static uint8_t  pattern2[] = { // the letter P, with some orientation frills
	0b11110001,
	0b10001000,
	0b10001000,
	0b11110000,

	0b10000000,
	0b10000000,
	0b10000001,
	0b10000010
};

void my_assert(uint32_t ok)
{
	if(ok) return;
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_3, GPIO_PIN_SET);
	while(1);
}
void app(void)
{
	//my_assert(false);
	uint8_t* pattern = pattern1;
	ledmat_init();
	while(1) {
		static int offset = 0; // for scrolling purposes
		for (int r = 0; r < 8; ++r) {
			int r1 = (r + offset) %8;
			led_set_row(r1, pattern[r]);
		}
		offset++;
		ledmat_show();
		HAL_Delay(50);
	}

}
