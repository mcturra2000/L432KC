#include "main.h"

const int fs = 44100;

void tim6_heartbeat(void)
{
	static int count = 0;
	if(count++ < fs/16)
		LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_8);
	else
		LL_GPIO_ResetOutputPin(GPIOA,  LL_GPIO_PIN_8);
	if(count == fs) count = 0;
}

void TIM6_DAC_IRQHandler(void)
{
	TIM6->SR = 0; // clear Update Interrupt Flag, which is the only flag it's got

	volatile static int count = 0;
	DAC1->DHR12R1 = 64*count++;
	tim6_heartbeat();
}
void app(void)
{
	// setup TIM6 for DAC
	RCC->APB1ENR1 |= RCC_APB1ENR1_TIM6EN;
	TIM6->ARR = SystemCoreClock / fs -1;
	TIM6->EGR |= TIM_EGR_UG; // generate update evt to rest timer and apply settings
	TIM6->CR1 |= TIM_CR1_CEN; // enable timer counter
	TIM6->DIER |= TIM_DIER_UIE; // enable update interrupt
	NVIC_EnableIRQ(TIM6_DAC_IRQn);

	// setup DAC
	RCC->APB1ENR1 |= RCC_APB1ENR1_DAC1EN; // enable DAC clock
	DAC1->CR |= DAC_CR_EN1; // enable it
	//DAC1->CR |= DAC_CR_TEN1; // enable trigger mode (by default on TIM6)

	//int count = 0;
	while(1) {

	}
}
