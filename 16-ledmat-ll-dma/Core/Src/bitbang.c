#include "main.h"



static void sda(int v)
{
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, v ? GPIO_PIN_SET : GPIO_PIN_RESET);
	if(v)
		GPIOA->BSRR |= GPIO_PIN_10;
	else
		GPIOA->BRR |= GPIO_PIN_10;
}


static void scl(int v)
{
	//HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, v ? GPIO_PIN_SET : GPIO_PIN_RESET);
	if(v)
		GPIOA->BSRR |= GPIO_PIN_9;
	else
		GPIOA->BRR |= GPIO_PIN_9;
}


static void gen_start(void) { 	sda(0); sda(0);}


static void send_bit(int v)
{
	scl(0);
	sda(v);
	scl(1);
}


static void ack(void)
{
	send_bit(1);
	scl(0);
}

static void gen_stop(void) { 	sda(0); scl(1); sda(1);}

static void send_byte_w_ack(uint8_t b)
{
	for(int i = 0; i < 8; i++) {
		scl(0);
		sda(b & 0b10000000);
		b <<= 1;
		scl(1);
	}
	ack();
}


void ledmat_bitbang_write(uint8_t sid, uint8_t *data, int n)
{
	gen_start();
	send_byte_w_ack(sid<<1);
	while(n--) send_byte_w_ack(*data++);
	gen_stop();
}
