#include "main.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define D10	GPIOA, LL_GPIO_PIN_11
#define D11	GPIOB, LL_GPIO_PIN_5
#define D12	GPIOB, LL_GPIO_PIN_4

extern UART_HandleTypeDef huart2;

bool fc_changed = true;
float fc = 1000;



void my_assert(uint32_t ok)
{
	if(ok != 0) return;
	__disable_irq();
	LL_GPIO_SetOutputPin(D11);
	while(1);
}

volatile float c0, c1;
volatile bool update_coeffs = true;
void TIM2_IRQHandler(void)
{
	TIM2->SR  = 0;

	volatile static uint32_t vout0 = 0, vout1 = 0;
	volatile static float loc_c0, loc_c1;

	if(update_coeffs) {
		update_coeffs = false;
		loc_c0 = c0;
		loc_c1 = c1;
	}
	uint32_t vin = RNG->DR >> 20; // in range [0, 4095)
	vout1 =  loc_c1 * vin + loc_c0 * vout0;
	my_assert(vout1 < 4096);
	DAC1->DHR12R1 = vout1;
	vout0 = vout1;
}

void app_systick_handler(void)
{
	volatile static uint32_t ticks = 0;

	if(ticks % 8 == 0) {
		const float fact = 1.01;
		bool changed = false;
		if(LL_GPIO_IsInputPinSet(D12) == 0 && fc < 32000) {
			changed = true;
			fc *= fact;
		}
		if(LL_GPIO_IsInputPinSet(D10) == 0 && fc > 10) {
			changed = true;
			fc /= fact;
		}

		if(changed || (ticks == 0)) {
			const float pi2 = 6.283185307179586;
			const float alpha = pi2 * fc / FS;
			c0 = 1.0/(1.0 + alpha);
			c1 = alpha * c0;
			fc_changed = true;
			update_coeffs = true;

		}
	}
	ticks ++;
}

void app(void)
{
	my_assert(NVIC_GetPriorityGrouping() == NVIC_PRIORITYGROUP_4);
	NVIC_SetPriority(SysTick_IRQn, 2); // low its priority so that TIM2 has first crack
	//my_assert(1==2);
	RCC->APB1ENR1 |= RCC_APB1ENR1_DAC1EN; // enable the DAC clock
	DAC1->CR |= DAC_CR_EN1; // enable DAC1

	RNG->CR |= RNG_CR_RNGEN; // enable random number hardware

	// perform extra init not done by CubeMX
	TIM2->ARR = SystemCoreClock/FS -1;
	NVIC_SetPriority(TIM2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(TIM2_IRQn);
	LL_TIM_EnableIT_UPDATE(TIM2);
	LL_TIM_EnableCounter(TIM2); // set the timer running

	//fc_changed = true;

	char msg[100];
	int n;
	n = snprintf(msg, 100, "\x1B[2J");
	HAL_UART_Transmit(&huart2, (uint8_t*) msg, n, 10);


	//printf("\x1B[2J"); // erase screen
	while(1) {
		//DAC1->DHR12R1 = RNG->DR >> 19;
		if(fc_changed) {
			fc_changed = false;
			int fc1 = fc;
			n = snprintf(msg, 100, "\x1B[HFreq %d \x1B[0K", fc1);
			HAL_UART_Transmit(&huart2, (uint8_t*) msg, n, 10);

		}

	}
}
