#include <string.h>

#include "cb.h"

bool cb_write(uint32_t value)
{
        if(cb_size == CB_CAP) return false;
        //__disable_irq();
        cb_data[cb_write_index] = value;
        if(++cb_write_index == CB_CAP) cb_write_index = 0;
        cb_size++;
        //__enable_irq();
        return true;
}

bool cb_read(uint32_t* value)
{
        *value = 0;
        if(cb_size == 0) return false;
        //__disable_irq();
        *value = cb_data[cb_read_index];
        if(++cb_read_index == CB_CAP) cb_read_index = 0;
        cb_size--;
        //__enable_irq();
        return true;
}

void cb_init(void)
{
	//inline volatile uint32_t cb_data[CB_CAP];
	cb_read_index = 0;
	cb_write_index = 0;
	cb_size = 0;
	  memset(cb_data, 0, sizeof(cb_data));
}
