// Found on
// https://deepbluembedded.com/stm32-dac-sine-wave-generation-stm32-dac-dma-timer-example/

#include "main.h"

#define NS  128

//uint32_t
uint16_t Wave_LUT[NS] = { 2048, 2149, 2250, 2350, 2450, 2549, 2646, 2742, 2837,
		2929, 3020, 3108, 3193, 3275, 3355, 3431, 3504, 3574, 3639, 3701, 3759,
		3812, 3861, 3906, 3946, 3982, 4013, 4039, 4060, 4076, 4087, 4094, 4095,
		4091, 4082, 4069, 4050, 4026, 3998, 3965, 3927, 3884, 3837, 3786, 3730,
		3671, 3607, 3539, 3468, 3394, 3316, 3235, 3151, 3064, 2975, 2883, 2790,
		2695, 2598, 2500, 2400, 2300, 2199, 2098, 1997, 1896, 1795, 1695, 1595,
		1497, 1400, 1305, 1212, 1120, 1031, 944, 860, 779, 701, 627, 556, 488,
		424, 365, 309, 258, 211, 168, 130, 97, 69, 45, 26, 13, 4, 0, 1, 8, 19,
		35, 56, 82, 113, 149, 189, 234, 283, 336, 394, 456, 521, 591, 664, 740,
		820, 902, 987, 1075, 1166, 1258, 1353, 1449, 1546, 1645, 1745, 1845,
		1946, 2047 };

#if 0
//extern DAC_HandleTypeDef hdac1;
//extern DMA_HandleTypeDef hdma_dac_ch1;
extern TIM_HandleTypeDef htim2;

void init_mda(void) {
//HAL_DMA_XFER_CPLT_CB_ID;
//HAL_DMA_XFER_HALFCPLT_CB_ID;
//HAL_DMA_RegisterCallback(&hdma_dac_ch1, HAL_DMA_XFER_HALFCPLT_CB_ID, my_dma_cplt);
//HAL_DMA_RegisterCallback(&hdma_dac_ch1, HAL_DMA_XFER_CPLT_CB_ID, my_dma_cplt);
//__HAL_DMA_ENABLE_IT()
//HAL_DMA_E

	LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_3, NS);
	LL_DMA_ConfigAddresses(DMA1, LL_DMA_CHANNEL_3, (uint32_t) Wave_LUT,
			(uint32_t) &DAC1->DHR12R1,
			LL_DMA_GetDataTransferDirection(DMA1, LL_DMA_CHANNEL_3));
	LL_DMA_EnableIT_HT(DMA1, LL_DMA_CHANNEL_3);
	LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_3);
	LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_3);
	LL_DAC_EnableDMAReq(DAC1, LL_DMA_CHANNEL_3);
	LL_DAC_Enable(DAC1, LL_DMA_CHANNEL_3);

//HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_1, (uint32_t*)Wave_LUT, 128, DAC_ALIGN_12B_R);
    HAL_TIM_Base_Start(&htim2);
}

#endif
